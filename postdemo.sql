/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : postdemo

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 30/03/2020 10:47:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for post_type
-- ----------------------------
DROP TABLE IF EXISTS `post_type`;
CREATE TABLE `post_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_type
-- ----------------------------
BEGIN;
INSERT INTO `post_type` VALUES (1, '加粗', 'font-weight:300;font-size:20px;', '2020-03-29 12:11:04');
INSERT INTO `post_type` VALUES (2, '红字', 'color:red;font-size:20px;', NULL);
INSERT INTO `post_type` VALUES (3, '斜体', 'font-style:italic;font-size:20px;', '2020-03-29 15:40:22');
INSERT INTO `post_type` VALUES (4, '背景图片', 'background-image:url(\'/statics/images/bj01.jpg\');font-size:20px;color:#FFF;', '2020-03-29 15:48:59');
COMMIT;

-- ----------------------------
-- Table structure for reply
-- ----------------------------
DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `subject_id` int(11) NOT NULL COMMENT '主题ID',
  `content` text COMMENT '回复内容',
  `sn` int(255) DEFAULT NULL COMMENT '帖子序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reply
-- ----------------------------
BEGIN;
INSERT INTO `reply` VALUES (1, 19, '234523452345', NULL, '2020-03-24 17:12:37');
INSERT INTO `reply` VALUES (2, 19, '234523452345', NULL, '2020-03-24 17:13:42');
INSERT INTO `reply` VALUES (3, 19, '234523452345', NULL, '2020-03-24 17:13:50');
INSERT INTO `reply` VALUES (4, 19, '234523452345', NULL, '2020-03-24 17:27:37');
INSERT INTO `reply` VALUES (5, 19, '123123435345646456456', NULL, '2020-03-24 17:28:50');
INSERT INTO `reply` VALUES (6, 19, '543645645645363456', NULL, '2020-03-24 17:29:57');
INSERT INTO `reply` VALUES (7, 19, '123123123123123123123123123', NULL, '2020-03-24 17:32:17');
INSERT INTO `reply` VALUES (8, 19, '12312342423435345345345', NULL, '2020-03-24 17:48:44');
INSERT INTO `reply` VALUES (9, 18, '2134234234345345345345', NULL, '2020-03-24 17:52:28');
INSERT INTO `reply` VALUES (10, 4, '12312312312312312', NULL, '2020-03-24 18:08:12');
INSERT INTO `reply` VALUES (11, 4, '12312312313312312321', NULL, '2020-03-24 18:09:09');
INSERT INTO `reply` VALUES (12, 22, '2312312312123123123123123', NULL, '2020-03-24 18:31:18');
INSERT INTO `reply` VALUES (13, 27, '1233242342424234234', NULL, '2020-03-30 09:58:31');
COMMIT;

-- ----------------------------
-- Table structure for subject
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '内容',
  `post_type` varchar(255) DEFAULT NULL COMMENT '帖子类型',
  `sort_weight` int(255) DEFAULT '1' COMMENT '排序权重',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subject
-- ----------------------------
BEGIN;
INSERT INTO `subject` VALUES (1, '测试标题1', '测试内容内容内容内容1', '1', 1, '2020-03-23 11:14:32');
INSERT INTO `subject` VALUES (2, '测试标题2', '测试内容内容内容内容2', '1', 1, '2020-03-23 11:15:27');
INSERT INTO `subject` VALUES (3, '测试标题3', '测试内容内容内容内容3', '1', 1, '2020-03-23 11:15:33');
INSERT INTO `subject` VALUES (4, '测试标题4', '测试内容内容内容内容4', '1', 1, '2020-03-23 11:15:36');
INSERT INTO `subject` VALUES (5, '测试标题5', '测试内容内容内容内容5', '1', 1, '2020-03-23 11:15:39');
INSERT INTO `subject` VALUES (6, '测试标题6', '测试内容内容内容内容6', '1', 1, '2020-03-23 11:15:44');
INSERT INTO `subject` VALUES (27, '123123', '32123123123123', '1', 1, '2020-03-29 13:09:24');
INSERT INTO `subject` VALUES (28, '测试斜体123', '123123', '2', 1, '2020-03-29 13:15:24');
INSERT INTO `subject` VALUES (29, '哈哈哈哈哈', '测试斜体', '3', 1, '2020-03-29 15:40:32');
INSERT INTO `subject` VALUES (30, '12321313', '123123123', '4', 1, '2020-03-29 15:49:12');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

package cn.net.taky.postdemo.manager;

import cn.net.taky.postdemo.entity.Subject;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ReplaceSubjectRender implements SubjectRender {
    @Override
    public String render(Subject subject, Map<String,Object> options) {
        return subject.getContent().replace("a","-");
    }
}

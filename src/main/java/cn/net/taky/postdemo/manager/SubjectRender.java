package cn.net.taky.postdemo.manager;

import cn.net.taky.postdemo.entity.Subject;

import java.util.Map;

public interface SubjectRender {
    String render(Subject subject, Map<String,Object> options);
}

package cn.net.taky.postdemo.manager;

public class NoSuchSubjectRenderException extends Exception {
    public NoSuchSubjectRenderException(String renderName) {
        super(String.format("Can not found render '%s'",renderName));
    }
}

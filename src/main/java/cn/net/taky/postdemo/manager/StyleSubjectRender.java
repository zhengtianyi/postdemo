package cn.net.taky.postdemo.manager;

import cn.net.taky.postdemo.entity.Subject;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class StyleSubjectRender implements SubjectRender {
    @Override
    public String render(Subject subject, Map<String,Object> options) {
        return String.format("<div style='%s'>%s</div>",options.get("style"),subject.getContent());
    }
}

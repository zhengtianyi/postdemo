package cn.net.taky.postdemo.manager;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SubjectRenderManager implements InitializingBean {
    private ApplicationContext applicationContext;
    private Map<String,SubjectRender> subjectRenderMap;

    @Autowired
    public SubjectRenderManager(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        this.subjectRenderMap = this.getApplicationContext().getBeansOfType(SubjectRender.class);
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
    public Map<String, SubjectRender> getSubjectRenderMap() {
        return subjectRenderMap;
    }

    public SubjectRender getRender(String renderName,boolean checkNull) throws NoSuchSubjectRenderException {
        SubjectRender result = this.getSubjectRenderMap().get(renderName);
        if(result == null && checkNull) {
            throw new NoSuchSubjectRenderException(renderName);
        }
        return result;
    }
    public SubjectRender getRender(String renderName) throws NoSuchSubjectRenderException {
        return this.getRender(renderName,true);
    }

}

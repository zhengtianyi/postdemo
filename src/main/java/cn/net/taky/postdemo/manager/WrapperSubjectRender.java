package cn.net.taky.postdemo.manager;

import cn.net.taky.postdemo.entity.Subject;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class WrapperSubjectRender implements SubjectRender {
    @Override
    public String render(Subject subject, Map<String,Object> options) {
        return String.format("<div>wrapper--%s--wrapper</div>",subject.getContent());
    }
}

package cn.net.taky.postdemo.service;

import cn.net.taky.postdemo.entity.Subject;
import cn.net.taky.postdemo.utils.PageParam;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 类名 ClassName  SubjectService
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 10:14 上午 ＞ω＜
 * 描述 Description TODO
 */
public interface SubjectService extends IService<Subject> {

    IPage<Subject> findByAll(PageParam pageParam);
}

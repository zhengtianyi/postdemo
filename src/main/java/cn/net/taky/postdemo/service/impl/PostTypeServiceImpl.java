package cn.net.taky.postdemo.service.impl;

import cn.net.taky.postdemo.entity.PostType;
import cn.net.taky.postdemo.entity.Reply;
import cn.net.taky.postdemo.mapper.PostTypeMapper;
import cn.net.taky.postdemo.mapper.ReplyMapper;
import cn.net.taky.postdemo.service.PostTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 类名 ClassName  PostTypeServiceImpl
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/29 12:15 下午 ＞ω＜
 * 描述 Description TODO
 */
@Service
public class PostTypeServiceImpl extends ServiceImpl<PostTypeMapper, PostType> implements PostTypeService {
}

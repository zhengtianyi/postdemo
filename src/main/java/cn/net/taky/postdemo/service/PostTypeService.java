package cn.net.taky.postdemo.service;

import cn.net.taky.postdemo.entity.PostType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 类名 ClassName  PostTypeService
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/29 12:14 下午 ＞ω＜
 * 描述 Description TODO
 */
public interface PostTypeService extends IService<PostType> {
}

package cn.net.taky.postdemo.service.impl;

import cn.net.taky.postdemo.entity.Subject;
import cn.net.taky.postdemo.mapper.SubjectMapper;
import cn.net.taky.postdemo.service.SubjectService;
import cn.net.taky.postdemo.utils.PageParam;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 类名 ClassName  SubjectServiceImpl
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 10:15 上午 ＞ω＜
 * 描述 Description TODO
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements SubjectService {

    @Autowired
    private SubjectMapper subjectMapper;

    @Override
    public IPage<Subject> findByAll(PageParam pageParam) {
        Subject subject = new Subject();
        if (pageParam.getQueryJson() != null) {
            subject = JSON.parseObject(pageParam.getQueryJson(), Subject.class);
        }
        IPage<Subject> byAll = subjectMapper.findByAll(new Page<>(pageParam.getPage(), pageParam.getRows()), subject, pageParam.getSord(), pageParam.getSidx());
        //分页
        return byAll;
    }
}

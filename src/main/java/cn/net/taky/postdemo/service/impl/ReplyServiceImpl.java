package cn.net.taky.postdemo.service.impl;

import cn.net.taky.postdemo.entity.Reply;
import cn.net.taky.postdemo.mapper.ReplyMapper;
import cn.net.taky.postdemo.service.ReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.relation.Role;


/**
 * 类名 ClassName  ReplyServiceImpl
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 10:09 上午 ＞ω＜
 * 描述 Description TODO
 */
@Service
public class ReplyServiceImpl extends ServiceImpl<ReplyMapper,Reply> implements ReplyService {

    @Autowired
    private ReplyMapper replyMapper;

}

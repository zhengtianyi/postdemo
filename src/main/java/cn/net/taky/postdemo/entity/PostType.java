package cn.net.taky.postdemo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 类名 ClassName  PostType
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/29 12:04 下午 ＞ω＜
 * 描述 Description TODO
 */
@Data
@TableName("post_type")
public class PostType {

    @TableId( type = IdType.AUTO)
    private int id;
    private String name;
    private String style;
    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

}

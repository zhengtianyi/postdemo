package cn.net.taky.postdemo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * subject
 * @author 
 */
@Data
@TableName("subject")
public class Subject  {
    /**
     * ID
     */
    @TableId( type = IdType.AUTO)
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 帖子类型
     */
    private String postType;

    /**
     * 内容
     */
    private String content;

    /**
     * 排序权重
     */
    private Integer sortWeight;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;



}
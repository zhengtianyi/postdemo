package cn.net.taky.postdemo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * reply
 * @author 
 */
@Data
@TableName("reply")
public class Reply {
    /**
     * id
     */
    @TableId( type = IdType.AUTO)
    private Integer id;

    /**
     * 主题ID
     */
    private Integer subjectId;

    /**
     * 帖子序号
     */
    private Integer sn;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 回复内容
     */
    private String content;

}
package cn.net.taky.postdemo.mapper;

import cn.net.taky.postdemo.entity.Reply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 类名 ClassName  ReplyMapper
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 10:11 上午 ＞ω＜
 * 描述 Description TODO
 */
public interface ReplyMapper extends BaseMapper<Reply> {
}

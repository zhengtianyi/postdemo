package cn.net.taky.postdemo.mapper;

import cn.net.taky.postdemo.entity.PostType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 类名 ClassName  PostTypeMapper
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/29 12:13 下午 ＞ω＜
 * 描述 Description TODO
 */
public interface PostTypeMapper extends BaseMapper<PostType> {
}

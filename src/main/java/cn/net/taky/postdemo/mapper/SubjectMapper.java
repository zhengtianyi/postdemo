package cn.net.taky.postdemo.mapper;

import cn.net.taky.postdemo.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 类名 ClassName  SubjectMapper
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 10:13 上午 ＞ω＜
 * 描述 Description TODO
 */
public interface SubjectMapper extends BaseMapper<Subject> {

    IPage<Subject> findByAll(Page<Subject> page, @Param("subject") Subject subject, @Param("sord") String sord, @Param("sidx") String sidx);
}

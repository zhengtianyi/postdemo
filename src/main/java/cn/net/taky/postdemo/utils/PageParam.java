package cn.net.taky.postdemo.utils;

import lombok.Data;

@Data
public class PageParam {

    /**
     * 当前页
     */
    private int page;

    /**
     * 每页记录数
     */
    private int rows;

    /**
     * 排序方式
     */
    private String sord;

    /**
     * 排序字段
     */
    private String sidx;

    /**
     * 查询条件
     */
    private String queryJson;

}

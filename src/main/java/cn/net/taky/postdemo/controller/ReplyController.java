package cn.net.taky.postdemo.controller;

import cn.net.taky.postdemo.entity.Reply;
import cn.net.taky.postdemo.service.ReplyService;
import cn.net.taky.postdemo.utils.PageParam;
import cn.net.taky.postdemo.utils.Result;
import cn.net.taky.postdemo.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 类名 ClassName  ReplyController
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 10:17 上午 ＞ω＜
 * 描述 Description TODO
 */
@CrossOrigin
@RestController
@RequestMapping("reply")
public class ReplyController {

    @Autowired
    private ReplyService replyService;

    // 根据subject ID 查询
    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") Integer id){
        List<Reply> lists = replyService.list(new QueryWrapper<Reply>().eq("subject_id", id).orderByDesc("create_time"));
        return new Result(ResultCode.SUCCESS,lists);
    }


    @GetMapping
    public Result findByAll(@RequestBody PageParam pageParam){
        Page<Reply> lists = replyService.page(new Page<>(pageParam.getPage(), pageParam.getRows()), new QueryWrapper<>());
        return new Result(ResultCode.SUCCESS,lists);
    }

    @PostMapping
    public Result add(@RequestBody Reply reply){
        boolean save = replyService.save(reply);
        if (save){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }


    @PutMapping
    public Result update(Reply reply){
        boolean update = replyService.updateById(reply);
        if (update){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Integer id){
        boolean remove = replyService.removeById(id);
        if (remove){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }


}

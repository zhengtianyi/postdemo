package cn.net.taky.postdemo.controller;

import cn.net.taky.postdemo.entity.PostType;
import cn.net.taky.postdemo.entity.Subject;
import cn.net.taky.postdemo.manager.SubjectRender;
import cn.net.taky.postdemo.manager.SubjectRenderManager;
import cn.net.taky.postdemo.service.PostTypeService;
import cn.net.taky.postdemo.service.SubjectService;
import cn.net.taky.postdemo.utils.PageParam;
import cn.net.taky.postdemo.utils.Result;
import cn.net.taky.postdemo.utils.ResultCode;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 类名 ClassName  Controller
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/23 9:55 上午 ＞ω＜
 * 描述 Description TODO
 */
@CrossOrigin
@RestController
@RequestMapping("/subject")
public class SubjectController {

    @Autowired
    protected SubjectService subjectService;
    @Autowired
    protected PostTypeService postTypeService;
    @Autowired
    protected SubjectRenderManager subjectRenderManager;

    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") Integer id) throws Exception {
        Subject subject = subjectService.getById(id);
        // 获取样式
        PostType postType = postTypeService.getById(subject.getPostType());
        SubjectRender subjectRender = this.subjectRenderManager.getRender(postType.getName());

        Map<String,Object> renderOptions = new LinkedHashMap<>();
        renderOptions.put("style",postType.getStyle());
        subject.setContent(subjectRender.render(subject,renderOptions));

        /*
        String style = postType.getStyle();

        String res = "<div style=" + style + ">" + subject.getTitle()  + "</div>";

        GroovyScriptEngine engine = new GroovyScriptEngine("src/main/java/cn/net/taky/postdemo/groovy");
        Script script = engine.createScript("type.groovy", new Binding());
        res =  (String) script.invokeMethod("run",res);
        subject.setTitle(res);*/

        return new Result(ResultCode.SUCCESS,subject);
    }

    @PostMapping("/list")
    public Result findByAll(@RequestBody PageParam pageParam) throws IllegalAccessException {
        IPage<Subject> byAll = subjectService.findByAll(pageParam);
        return new Result(ResultCode.SUCCESS,byAll);
    }

    @PostMapping
    public Result add(@RequestBody Subject subject){
        boolean save = subjectService.save(subject);
        if (save){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }

    @PutMapping
    public Result update(Subject subject){
        boolean update = subjectService.updateById(subject);
        if (update){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Integer id){
        boolean remove = subjectService.removeById(id);
        if (remove){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }
    }



    @RequestMapping("/editScript")
    public Result eidtScript(String script) {




        return new Result(ResultCode.SUCCESS);
    }
}

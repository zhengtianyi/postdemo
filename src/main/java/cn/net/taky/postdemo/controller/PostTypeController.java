package cn.net.taky.postdemo.controller;

import cn.net.taky.postdemo.entity.PostType;
import cn.net.taky.postdemo.service.PostTypeService;
import cn.net.taky.postdemo.utils.Result;
import cn.net.taky.postdemo.utils.ResultCode;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URL;
import java.util.List;

/**
 * 类名 ClassName  PostTypeController
 * 项目 ProjectName  postdemo
 *
 * @author 郑添翼 Taky.Zheng
 * 邮箱 E-mail 275158188@qq.com
 * 时间 Date  2020/3/29 12:19 下午 ＞ω＜
 * 描述 Description TODO
 */
@CrossOrigin
@RestController
@RequestMapping("/postType")
public class PostTypeController {

    @Autowired
    private PostTypeService postTypeService;


    @GetMapping
    public Result list(){
        List<PostType> list = postTypeService.list();
        return new Result(ResultCode.SUCCESS, list);
    }

    @PostMapping
    public Result save(@RequestBody PostType postType){
        boolean save = postTypeService.save(postType);
        if (save){
            return new Result(ResultCode.SUCCESS);
        }else {
            return new Result(ResultCode.FAIL);
        }

    }


    private boolean saveScript(String script){
       // FileUtils.toFile(new URL("/"));


        return true;
    }



}
